var http = require('http'),
    fs = require('fs'),
    request = require('request'),
    AdmZip = require('adm-zip'),
    file = fs.createWriteStream('./public/zip/file.zip'); 

var req = request(
    {
        method: 'GET',
        uri: 'http://ncert.nic.in/textbook/pdf/ahhn1dd.zip'
    }
);

req.pipe(file);
req.on('end', function() {
    var zip = new AdmZip("./public/zip/file.zip"),
    zipEntries = zip.getEntries();
    zip.extractAllTo(/*target path*/"./public/unzip/", /*overwrite*/true);
});


