// Using 'express js' middleware
const express       = require('express');
const bodyParser    = require('body-parser');

// Create a server from 'express js'
const server    = express();

// Add body parser to server (adding body parser middleware)
server.use(bodyParser.urlencoded({ extended: false }));

// Asking to parse the input with application/json format
server.use(bodyParser.json());

// Add routes to the server
server.get( '/', function(request, response){
    response.send({"message":"Server is running successfully."})
} )

server.post( '/todo', function(request, response){
    let todoTask = request.body;
    response.send({
        "message": "Received the todo task successfully",
        "givenTodoTask": todoTask
    });
} )

server.listen(3000, function(){
    console.log("Server started at port 3000")
})

