# crud_app
CRUD App using NodeJs, ExpressJs, MongoDB

# Install Express generator

Install npm module express generator globally

`$ npm install -g express-generator`

`$ express`
`$ npm install mongoose`

# Installation

Do `$ npm install`

Run `node server.js`

You application will run on `port: 3000`

API:
GET:    /api/users/user/:username

POST:     /api/users

PUT:     /api/users/updatebyid

PUT:     /api/users/update

DELETE: /api/users/delete