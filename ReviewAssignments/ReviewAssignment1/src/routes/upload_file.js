const express = require("express");
const router = express.Router();
const indexController = require("../controllers/index");
const uploadController = require("../controllers/upload");

let routes = app => {
  router.get("/", indexController.getIndex);

  router.post("/upload", uploadController.uploadFile);

  return app.use("/", router);
};

module.exports = routes;