## Goal: Implement a node js REST API to upload single file.

## Input: Single file; format: pdf, xlsx, docx, pptx, txt

## Output: 
        1. The file should be stored in a folder in server(where the service is up)
        2. The file details should be stored in MongoDB

## Method: POST

## Install npm modules
`$ npm install --save express`
`$ npm install mongoose`
`$ npm install multer`
`$ npm install multer-gridfs-storage`

## Multer:
```
        Multer is middleware for Express and Node.js that makes it easy to handle this multipart/form-data when your users upload files. 
```


## Multer-gridfs-storage:
```
        GridFS storage engine for Multer to store uploaded files directly to MongoDb
```

## How to run the server?

```
> node server.js

```

## How to access the application 

```
http://localhost:3000/

```

## UI to be provided:
```
1. Choose file
2. On hit submit button , upload the file & display on the page is file has been uploaded

```
