const util = require("util");
const multer = require("multer");
//const GridFsStorage = require("multer-gridfs-storage");

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  file: (req, file) => {
    const match = ["file/txt", "file/pdf", "file/ppt", "file/xlsx", "file/docx"];

    if (match.indexOf(file.mimetype) === -1) {
      const filename = `${Date.now()}-${file.originalname}`;
      return filename;
    }

    return {
      bucketName: "updateFile",
      filename: `${Date.now()}-${file.originalname}`
    };
  }
});

var updateFile = multer({ storage: storage}).single("file");
var updateFilesMiddleware = util.promisify(updateFile);
module.exports = updateFilesMiddleware;
