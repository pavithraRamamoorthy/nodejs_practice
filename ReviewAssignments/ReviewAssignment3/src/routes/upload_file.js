const express = require("express");
const router = express.Router();
const indexController = require("../controllers/index");
const uploadController = require("../controllers/upload");
const path = require('path');
const downloadController = require("../controllers/download");
const updateController = require("../controllers/update");
const updateFileController = require("../controllers/updateFile");

let routes = app => {
  router.get("/", indexController.getIndex);

  router.post("/upload", uploadController.uploadFile);

  router.get("/download", downloadController.downloadFile);

  router.put("/update", updateController.updateFile);

  router.put("/update/:file", updateFileController.updateFile);

  return app.use("/", router);
};

module.exports = routes;