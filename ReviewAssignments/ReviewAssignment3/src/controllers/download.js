const fs 		 = require('fs');

const folderPath 		= "./uploads/";

const downloadFile = async (request, response) => {
        var fileName = request.query.fileName;
        response.download(folderPath + fileName);
}

module.exports = {
  downloadFile: downloadFile
};