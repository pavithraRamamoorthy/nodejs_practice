const update = require("../middleware/upload");

const updateFile = async (req, res) => {
  try {
    await update(req, res);

    console.log(req.file);
    if (req.file == undefined) {
      return res.send(`You must select a file.`);
    }
    return res.send(`File has been updated.`);
  } catch (error) {
    console.log(error);
    return res.send(`Error when trying update file: ${error}`);
  }
};

module.exports = {
  updateFile: updateFile
};