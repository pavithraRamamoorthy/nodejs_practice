const express = require("express");
const router = express.Router();
const indexController = require("../controllers/index");
const uploadController = require("../controllers/upload");
const path = require('path');
const downloadController = require("../controllers/download");

let routes = app => {
  router.get("/", indexController.getIndex);

  router.post("/upload", uploadController.uploadFile);

  router.get("/download", downloadController.downloadFile);

  return app.use("/", router);
};

module.exports = routes;