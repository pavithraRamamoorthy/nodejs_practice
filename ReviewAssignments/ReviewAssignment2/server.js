const express = require("express");
const app = express();
const Routes = require("./src/routes/upload_file");

app.use(express.urlencoded({ extended: true }));
Routes(app);

let port = 3000;
app.listen(port, () => {
  console.log(`Running at localhost:${port}`);
});