## Goal: Implement a node js REST API to retrieve the file..

## Input: File Id or FileName

## Output: 
        1. The file should be downloaded

## Method: GET

## Install npm modules
`$ npm install --save express`
`$ npm install mongoose`
`$ npm install multer`

## Multer:
```
        Multer is middleware for Express and Node.js that makes it easy to handle this multipart/form-data when your users upload files. 
```

## How to run the server?

```
> node server.js

```

## How to access the application 

```
http://localhost:3000/

htt://localhost:3000/download/?fileName=(***)
```

## UI to be provided:
```
1. Choose file
2. On hit submit button , upload the file & display on the page is file has been uploaded

```
