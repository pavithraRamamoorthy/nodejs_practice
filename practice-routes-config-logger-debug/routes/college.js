const express = require('express');
const router = express();

var path        = require('path');

// use 'public' folder that holds the static files
var public      = path.join(__dirname, 'public/html');

// map the public folder in middleware
router.use(express.static(public));

// serve(render) the file on route
router.get('/collegeDetails', function(request, response){
        response.sendFile(path.join(public, 'college.html'));
        //response.json({"Colleges" : })
})

module.exports = router