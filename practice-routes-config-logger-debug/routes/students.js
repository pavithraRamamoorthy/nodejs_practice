const express = require('express');
const router = express();

var path        = require('path');

// use 'public' folder that holds the static files
var public      = path.join(__dirname, './public/html');

// map the public folder in middleware
router.use(express.static(public));

// serve(render) the file on route
router.get('/studentDetails', function(request, response){
        response = response.sendFile(path.join(public, 'student.html'));
})

//named and query parameters
router.get('/:id', function (request, response) {
        var studentName = request.query.studentName;
        var studentId = request.params.id
        response.json( { "message": 
        {
                "The student name is " : studentName ,
                "The student id is "   : studentId
        }        
        } )
        console.log("The user has received the studentId and studentname")
})
      

module.exports = router