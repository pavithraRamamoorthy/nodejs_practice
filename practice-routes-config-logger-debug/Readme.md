
## Installing node modules

```
> npm install --save express
> npm install --save winston
> npm install --save config

```

## How to run the server?

```
> npm install

> npm run start

```

## How to debug the server?

```
> npm run debug

```

## Access the application 

```
    http://localhost:3000/college/collegeDetails

    http://localhost:3000/student/studentDetails

    http://localhost:3000/department/departmentDetails

    http://localhost:3000/student/101?studentName=pavithra
```
