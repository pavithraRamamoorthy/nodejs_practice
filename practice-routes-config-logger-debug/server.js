const express = require('express');
const server = express();
const logger    = require('./utils/logger')
const config    = require('config')

const college = require('./routes/college');
const department = require('./routes/departments');
const student = require('./routes/students');


server.use(function(request, response, next){
    logger.info("Request Body ", request.body)
    logger.info("Request Query ", request.query)
    logger.info("Request Headers ", request.headers)
    let responseData = response.send;
    response.send   = function(data){
        logger.info(JSON.parse(data))
        responseData.apply(response, arguments)
    }    
    next();
})

server.use('/college', college);
server.use('/department', department);
server.use('/student', student);


let port = config.get('port')

if(port){
        server.listen(port, function(){
            logger.info("Server is listening at port : " + port);
        })
}else{
        logger.error("No servert port is set. Check the config/Env setup.")
    }

// server.listen(3000, function()
// {
//         console.log("Server started at port is 3000");
// })