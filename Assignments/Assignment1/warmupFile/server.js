const express = require('express');
const bodyParser = require('body-parser');
var server =  express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: true}));

server.get('/employee', function(request, response) {
    response.send({"message":"Welcome", 
    "employee": {"firstName":"Raju", "lastName":"R", "id":"678978"} });
})

server.post('/employee', (request, response) => {
    console.log(request.body);
    response.send({"message":`Welcome ${request.body.firstName}`});
})

server.listen(3000, function(){
            console.log("Server started at port 3000")
        })