Build a node js server with below http methods:

- GET, 
i/p: /employee
o/p: respond with { "message":"Welcome", "employee": {"firstName":"Raju", "lastName":"R", "id":"678978"} }

- POST,
route: /employee 
i/p: {"firstName":"Raju", "lastName":"R", "id":"678978"}
o/p: respond with {"message":"Welcome Raju"}
