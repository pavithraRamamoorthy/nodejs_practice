const express 	 = require("express");
const server	 = express();
const fs 	 = require('fs');
const http      = require('https');

// Route for file download
server.get('/download',function(request, response){

const file = fs.createWriteStream("./public/file.jpg");
http.get("https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg", function(response) {
  response.pipe(file);
});

})

server.listen(3000, function(){
        console.log("Server started at port 3000")
    })