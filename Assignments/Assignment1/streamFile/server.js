
// Goal: To stream a video file

const express   = require('express');

// to help serve a local video file
const fs        = require('fs');
const PATH      = "./public/assets/videos/";
const server    = express();

// Create an instance of the http server to handle HTTP requests
server.get('/audio', function(request, response){
    // Set a response type of mp4 video for the response
    response.writeHead(200, {'Content-Type': 'audio/mp3'});

    // Read the video into a stream
    var videoStream = fs.createReadStream( PATH + 'audio.mp3');
    
    // Pipe our stream into the response
    videoStream.pipe(response);
});

server.get('/wav', function(request, response){
        // Set a response type of mp4 video for the response
        response.writeHead(200, {'Content-Type': 'audio/wav'});
    
        // Read the video into a stream
        var videoStream = fs.createReadStream( PATH + 'wavfile.wav');
        
        // Pipe our stream into the response
        videoStream.pipe(response);
    });

server.get('/ogg', function(request, response){
    // Set a response type of mp4 video for the response
    response.writeHead(200, {'Content-Type': 'audio/ogg'});

    // Read the video into a stream
    var videoStream = fs.createReadStream( PATH + 'oggfile.ogg');
    
    // Pipe our stream into the response
    videoStream.pipe(response);
});    

server.listen(3000, function(){
        console.log("Server started successfully.")
});

