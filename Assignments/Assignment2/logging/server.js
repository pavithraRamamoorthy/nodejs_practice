
const express   = require('express')
const server    = express()
const logger    = require('./utils/logger')
const uuid = require('uuid');
console.log(uuid)

/* Set the logger for entire application to log the request body */
server.use(function(request, response, next){
    logger.info("Request Body ", request.body,uuid.v4())
    logger.info("Request Query ", request.query,uuid.v4())
    logger.info("Request Headers ", request.headers,uuid.v4())
    let responseData = response.send;
    response.send   = function(data){
        logger.info(JSON.parse(data))
        responseData.apply(response, arguments)
    }    
    next();
    
})

const student = require('./routes/student')
server.use('/student', student)

server.listen(3000, function(){
    logger.info("Server is started at port 3000");
})