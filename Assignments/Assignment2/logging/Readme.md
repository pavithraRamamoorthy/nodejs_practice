
## Installing node modules

```
> npm install --save express
> npm install --save winston
> npm install --save uuid

```

## How to run the server?

```
> npm install

> npm run start

```

## Access the application 

```
    http://localhost:3000/student/about

    http://localhost:3000/student/10?studetName=pavithra
```
