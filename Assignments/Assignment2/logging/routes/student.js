var express = require('express')
var router = express.Router()

router.get('/about', function (request, response) {
  response.json( { "studentDetails": 
        {
                "studentId" : 1,
                "firstName" : "Pavithra",
                "lastName" : "Ramamoorthy", 
                "college" : "SVCET",
                "department" : "MCA"
        } 
} )
})

router.get('/:id', function (request, response) {
  var studetName = request.query.studetName;
  response.json( { "message": "The student name is  " + studetName } )
})

 
module.exports = router