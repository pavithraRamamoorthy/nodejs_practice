// Using 'express js' middleware
const express       = require('express');
const bodyParser    = require('body-parser');

// Create a server from 'express js'
const server    = express();

// Add body parser to server
server.use(bodyParser.urlencoded({ extended: false }));

//Pparse the input with json format
server.use(bodyParser.json());

// Add routes to the server
server.get( '/books', function(request, response){
        response.send({"books":
        {
                "book1": "NodeJs",
                "book2": "JavaScript",
                "book3": "Python",
                "book4": "Mysql",
                "book5": "MongoDB",
                
        }
})
    } )


server.post( '/library', function(request, response){
        let books = request.body;
        response.send({
                "message": "Received the book successfully from library",
                "givenBooks": books
        });
    } )


server.listen(3000, function(){
    console.log("Server started at port 3000")
})

