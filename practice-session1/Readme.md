
## Pre-requisites

```
Node version = 14.2.0

```


## How to run the server?

```
> node server.js

```

## How to access the application 

```
http://localhost:3000/

```

## Installing node modules

```
> npm install --save moduleName

```
